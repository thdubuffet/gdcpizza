# GDC Pizza

Pizza management and ingredients

## Installation

Clone the project on local

```bash
composer install
yarn install
```

## Usage

Create database
```python
bin/console doctrine:database:create
```


Create tables with doctrine migrations
```python
bin/console doctrine:migrations:migrate

```


Load example data
```python
bin/console doctrine:fixtures:load
```

## Website

Pizza Management - Go to [/pizza](https://127.0.0.1:8000/pizza/)

Ingredient Management - Go to [/ingredient](https://127.0.0.1:8000/ingredient/)