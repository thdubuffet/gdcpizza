<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PizzaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Pizza
{

    /**
     * Percentage of preparation
     */
    const PERCENTAGE_COST_OF_PREPARATION = 50;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\Length(min="5", max="60")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min="5", max="400")
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PizzaIngredient", mappedBy="pizza", cascade={"persist", "remove"})
     * @Assert\Count(min="2", minMessage="You need two ingredients")
     */
    private $ingredients;

    /**
     * Pizza constructor.
     */
    public function __construct()
    {
        $this->ingredients = new ArrayCollection();
    }

    /**
     * get id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * set Name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * set name
     *
     * @param string $name
     * @return Pizza
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * get description
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * set description
     *
     * @param string|null $description
     * @return Pizza
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * get price
     *
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param $price
     * @return Pizza
     */
    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Generate a price calculate with the cost of the ingredients and the preparation of the pizza
     * @ORM\PreFlush()
     * @return Pizza
     */
    public function generatePrice(): self
    {
        $sum = 0;

        foreach ($this->getIngredients() as $i) {
            $sum += $i->getIngredient()->getCost();
        }

        $sum += $sum * self::PERCENTAGE_COST_OF_PREPARATION/100;

        $this->price = $sum;

        return $this;
    }

    /**
     * @return Collection|PizzaIngredient[]
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    /**
     * Add new pizza ingredient
     *
     * @param PizzaIngredient $ingredient
     * @return Pizza
     */
    public function addIngredient(PizzaIngredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
            $ingredient->setPizza($this);
        }

        return $this;
    }

    /**
     * Remove pizza ingredient
     *
     * @param PizzaIngredient $ingredient
     * @return Pizza
     */
    public function removeIngredient(PizzaIngredient $ingredient): self
    {
        if ($this->ingredients->contains($ingredient)) {
            $this->ingredients->removeElement($ingredient);
        }

        return $this;
    }
}
