<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IngredientRepository")
 */
class Ingredient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\Length(min="5", max="60")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min="5", max="400")
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $cost;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PizzaIngredient", mappedBy="ingredient")
     */
    private $pizzaIngredient;


    /**
     * Ingredient constructor.
     */
    public function __construct()
    {
        $this->pizzaIngredient = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * get Name
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Ingredient
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * set Description
     *
     * @param string $description
     * @return Ingredient
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get cost
     *
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set Cost, is updated if cost changed
     *
     * @param $cost
     * @return Ingredient
     */
    public function setCost($cost): self
    {

        if ($this->cost !== $cost && $this->pizzaIngredient->count() > 0) {
            // New cost, update pizza price !

            foreach ($this->pizzaIngredient as $pizzaIngredient) {
                $pizzaIngredient->getPizza()->generatePrice();
            }
        }

        $this->cost = $cost;

        return $this;
    }



    /**
     * get PizzaIngredient
     *
     * @return Collection|PizzaIngredient[]
     */
    public function getPizzaIngredient(): Collection
    {
        return $this->pizzaIngredient;
    }

    /**
     * Add pizza ingredient
     *
     * @param PizzaIngredient $ingredient
     * @return Ingredient
     */
    public function addPizzaIngredient(PizzaIngredient $ingredient): self
    {
        if (!$this->pizzaIngredient->contains($ingredient)) {
            $this->pizzaIngredient[] = $ingredient;
            $ingredient->setIngredient($this);
        }

        return $this;
    }

    /**
     * Remove pizza ingredient
     *
     * @param PizzaIngredient $ingredient
     * @return Ingredient
     */
    public function removePizzaIngredient(PizzaIngredient $ingredient): self
    {
        if ($this->pizzaIngredient->contains($ingredient)) {
            $this->pizzaIngredient->removeElement($ingredient);
        }

        return $this;
    }

    /**
     * Return custom name of class
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name . ' - ' . $this->cost;
    }
}
