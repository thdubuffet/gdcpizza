<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PizzaIngredientsRepository")
 */
class PizzaIngredient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pizza", inversedBy="ingredients", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pizza;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ingredient", inversedBy="pizzaIngredient", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ingredient;

    /**
     * @ORM\Column(name="position", nullable=false, type="integer")
     */
    private $position = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Pizza
     */
    public function getPizza(): Pizza
    {
        return $this->pizza;
    }


    /**
     * @param Pizza $pizza
     * @return PizzaIngredient
     */
    public function setPizza(?Pizza $pizza): self
    {
        $this->pizza = $pizza;

        return $this;
    }

    /**
     * @return Ingredient
     */
    public function getIngredient(): ?Ingredient
    {
        return $this->ingredient;
    }

    /**
     * @param Ingredient $ingredient
     * @return PizzaIngredient
     */
    public function setIngredient(Ingredient $ingredient): self
    {
        $this->ingredient = $ingredient;

        $ingredient->addPizzaIngredient($this);

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return PizzaIngredient
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;
        return $this;
    }
}
