<?php

namespace App\Form;

use App\Entity\Pizza;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PizzaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', TextType::class)

            ->add('description', TextareaType::class)

            ->add('ingredients', CollectionType::class, [
                'entry_type'        => PizzaIngredientType::class,
                'allow_add'         => true,
                'allow_delete'      => true,
                'prototype'         => true,
                'required'          => true,
                'delete_empty'      => true,
                'attr'              => [
                    'class' => "pizza-ingredients-collection",
                ]
            ])

            ->add('submit',  SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pizza::class,
        ]);
    }
}
