<?php

namespace App\Form;

use App\Entity\Ingredient;
use App\Entity\PizzaIngredient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PizzaIngredientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ingredient', EntityType::class, [
                'class' => Ingredient::class
            ])
            ->add('position', HiddenType::class, [
                'attr' => [
                    'class' => 'position-ingredient',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PizzaIngredient::class
        ]);
    }


    public function getBlockPrefix()
    {
        return 'PizzaIngredientType';
    }

}
