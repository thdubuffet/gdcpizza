<?php

namespace App\Controller;

use App\Entity\Pizza;
use App\Form\PizzaType;
use App\Repository\PizzaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pizza", name="pizza_")
 */
class PizzaController extends AbstractController
{
    /**
     * List of pizza
     *
     * @Route("/", name="index")
     * @Template()
     * @param PizzaRepository $pizzaRepository
     * @return array
     */
    public function index(PizzaRepository $pizzaRepository)
    {

        $pizzas = $pizzaRepository->findAll();

        return [
            'pizzas' => $pizzas
        ];
    }

    /**
     * Add a new pizza
     *
     * @Route("/add", name="add")
     * @Template()
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function add(Request $request, EntityManagerInterface $entityManager)
    {

        $form = $this->createForm(PizzaType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $entityManager->persist($form->getData());
                $entityManager->flush();

                $this->addFlash('success', 'Add new pizza with success');

                return $this->redirectToRoute('pizza_index');
            }

        }

        return [
            'formPizza' => $form->createView()
        ];
    }


    /**
     * Edit a pizza
     *
     * @Route("/edit/{id}", name="edit")
     * @Template()
     * @param Pizza $pizza
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Pizza $pizza, Request $request, EntityManagerInterface $entityManager)
    {

        $form = $this->createForm(PizzaType::class, $pizza);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $entityManager->persist($form->getData());
                $entityManager->flush();

                $this->addFlash('success', 'Add new pizza with success');

                return $this->redirectToRoute('pizza_index');
            }

        }

        return [
            'formPizza' => $form->createView()
        ];
    }


    /**
     * Delee a pizza
     *
     * @Route("/delete/{id}", name="delete")
     * @Template()
     * @param Pizza $pizza
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Pizza $pizza, EntityManagerInterface $entityManager)
    {


        $entityManager->remove($pizza);
        $entityManager->flush();

        $this->addFlash('danger', 'Pizza deleted !');

        return $this->redirectToRoute('pizza_index');
    }
}
