<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Form\IngredientType;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ingredient", name="ingredient_")
 */
class IngredientController extends AbstractController
{
    /**
     * List of pizza
     *
     * @Route("/", name="index")
     * @Template()
     * @param IngredientRepository $ingredientRepository
     * @return array
     */
    public function index(IngredientRepository $ingredientRepository)
    {

        $ingredients = $ingredientRepository->findAll();

        return [
            'ingredients' => $ingredients
        ];
    }

    /**
     * Add a new ingredient
     *
     * @Route("/add", name="add")
     * @Template()
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function add(Request $request, EntityManagerInterface $entityManager)
    {

        $form = $this->createForm(IngredientType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $entityManager->persist($form->getData());
                $entityManager->flush();

                $this->addFlash('success', 'Add new ingredient with success');

                return $this->redirectToRoute('ingredient_index');
            }

        }

        return [
            'formIngredient' => $form->createView()
        ];
    }

    /**
     * Edit a ingredient
     *
     * @Route("/edit/{id}", name="edit")
     * @Template()
     * @param Ingredient $ingredient
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Ingredient $ingredient, Request $request, EntityManagerInterface $entityManager)
    {

        $form = $this->createForm(IngredientType::class, $ingredient);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $entityManager->persist($form->getData());
                $entityManager->flush();

                $this->addFlash('success', 'Add new ingredient with success');

                return $this->redirectToRoute('ingredient_index');
            }

        }

        return [
            'formIngredient' => $form->createView()
        ];
    }

    /**
     * delete ingredient
     *
     * @Route("/delete/{id}", name="delete")
     * @Template()
     * @param Ingredient $ingredient
     * @param EntityManagerInterface $entityManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Ingredient $ingredient, EntityManagerInterface $entityManager)
    {

        if ($ingredient->getPizzaIngredient()->count() === 0) {

            $entityManager->remove($ingredient);
            $entityManager->flush();

            $this->addFlash('success', 'Ingredient deleted');

        } else {
            $this->addFlash('danger', 'This ingredient is used');
        }

        return $this->redirectToRoute('ingredient_index');
    }
}
