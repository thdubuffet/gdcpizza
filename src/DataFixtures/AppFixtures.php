<?php

namespace App\DataFixtures;

use App\Entity\Ingredient;
use App\Entity\Pizza;
use App\Entity\PizzaIngredient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $ingredients = $this->createIngredients($manager);

        $this->createPizzas($ingredients, $manager);

        $manager->flush();
    }

    /**
     * Create Pizza
     *
     * @param array $ingredients
     * @param ObjectManager $manager
     */
    private function createPizzas(array $ingredients, ObjectManager $manager)
    {

        dump('la');
        for($i=0; $i<10; $i++) {

            $totalIngredients   = rand(2, 6);
            $nameAndDescription = "GDC PIZZA $i with $totalIngredients ingredients";
            $currentIngredient  = array_rand($ingredients, $totalIngredients);

            $pizza = (new Pizza())
                ->setName($nameAndDescription)
                ->setDescription($nameAndDescription)
            ;

            foreach ($currentIngredient as $key => $ci) {
                $pizzaIngredient = (new PizzaIngredient())
                    ->setIngredient($ingredients[$ci])
                    ->setPosition($key)
                ;

                $pizza->addIngredient($pizzaIngredient);
            }

            $manager->persist($pizza);

        }

    }


    /**
     * List of ingredients | 8 ingredients
     *
     * @param ObjectManager $manager
     * @return array
     */
    private function createIngredients(ObjectManager $manager): array
    {
        $ingredients = [];

        $ingredients[] = (new Ingredient())
            ->setName('Tomate')
            ->setDescription('Sauce tomate Heinz')
            ->setCost(1.42)
        ;

        $ingredients[] = (new Ingredient())
            ->setName('Fromage')
            ->setDescription('Fromage a pizza')
            ->setCost(1)
        ;

        $ingredients[] = (new Ingredient())
            ->setName('Fromage à raclette')
            ->setDescription('Fromage à raclette pizza')
            ->setCost(2)
        ;

        $ingredients[] = (new Ingredient())
            ->setName('Champignon')
            ->setDescription('Champignon frais de GDC')
            ->setCost(3)
        ;

        $ingredients[] = (new Ingredient())
            ->setName('Jambon')
            ->setDescription('Jambon de GDC')
            ->setCost(3)
        ;

        $ingredients[] = (new Ingredient())
            ->setName('Saumon')
            ->setDescription('Saumon frais de norvége')
            ->setCost(20)
        ;

        $ingredients[] = (new Ingredient())
            ->setName('Truite')
            ->setDescription('Truite de norvége')
            ->setCost(14)
        ;

        $ingredients[] = (new Ingredient())
            ->setName('Pomme de terre')
            ->setDescription('Pomme de terre')
            ->setCost(1)
        ;

        foreach ($ingredients as $ingredient) {
            $manager->persist($ingredient);
        }

        return $ingredients;
    }
}
